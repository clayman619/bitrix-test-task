<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Data\Cache;

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$search = $request->getQuery('search');

if (!$search)
    return;

$cache = Cache::createInstance();
$cache_key = $arParams['IBLOCK_ID'] . $search;

if ($cache->initCache(7200, $cache_key)) {
    $arResult = $cache->getVars();
}
elseif ($cache->startDataCache()) {
    if (!\Bitrix\Main\Loader::includeModule("iblock"))
        return;

    $query = \Bitrix\Iblock\ElementTable::getList([
        'select' => ['ID', 'NAME', 'IBLOCK_SECTION_ID'],
        'filter' => ['IBLOCK_ID' => $arParams['IBLOCK_ID'], '%NAME' => $search]
    ]);

    $sectionIds = [];

    while($result = $query->fetch()) {

        if(!in_array($sectionIds, $result['IBLOCK_SECTION_ID']))
            $sectionIds[] = $result['IBLOCK_SECTION_ID'];

        $elements[] = $result;
    }

    $section_query = \Bitrix\Iblock\SectionTable::getList([
        'select' => [
            'ID', 'NAME',
        ],
        'filter' => [
            '=ID' => $sectionIds,
        ]
    ]);

    while($section = $section_query->fetch()){
        $sections[$section['ID']] = $section['NAME'];
    }

    foreach($elements as $element)
        $arResult['ITEMS'][] = [
            'NAME' => $element['NAME'],
            'SECTION_NAME' => $sections[$element['IBLOCK_SECTION_ID']
            ]
        ];

    $cache->endDataCache($arResult);
}

$this->IncludeComponentTemplate();