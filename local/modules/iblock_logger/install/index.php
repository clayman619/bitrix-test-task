<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class iblock_logger extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();
        
        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        
        $this->MODULE_ID = 'iblock_logger';
        $this->MODULE_NAME = Loc::getMessage('IBLOCK_LOGGER_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('IBLOCK_LOGGER_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        \Bitrix\Main\Loader::includeModule("highloadblock");
        $result = Bitrix\Highloadblock\HighloadBlockTable::add(array(
            'NAME' => 'Iblocklogger',
            'TABLE_NAME' => 'iblocklogger',
        ));

        $highLoadBlockId = $result->getId();

        $userTypeEntity = new CUserTypeEntity();
        $fields = [
            'string' => ['USER_ID'],
            'iblock_element' => ['ELEMENT'],
            'datetime' => ['TIME']
        ];
        foreach ($fields as $type => $typeArrs) {
            foreach ($typeArrs as $typeArr) {
                $userTypeData = array(
                    "ENTITY_ID" => "HLBLOCK_" . $highLoadBlockId,
                    "FIELD_NAME" => "UF_" . $typeArr,
                    "USER_TYPE_ID" => $type,
                    "XML_ID" => "XML_ID_" . $typeArr,
                    "SORT" => 100,
                    "MULTIPLE" => "N",
                    "MANDATORY" => "N",
                    "SHOW_FILTER" => "N",
                    "SHOW_IN_LIST" => "",
                    "EDIT_IN_LIST" => "",
                    "IS_SEARCHABLE" => "N",
                    "SETTINGS" => array(
                        "DEFAULT_VALUE" => "",
                        "SIZE" => "20",
                        "ROWS" => "1",
                        "MIN_LENGTH" => "0",
                        "MAX_LENGTH" => "0",
                        "REGEXP" => "",
                    ),
                    "EDIT_FORM_LABEL" => array(
                        "ru" => "",
                        "en" => "",
                    ),
                    "LIST_COLUMN_LABEL" => array(
                        "ru" => "",
                        "en" => "",
                    ),
                    "LIST_FILTER_LABEL" => array(
                        "ru" => "",
                        "en" => "",
                    ),
                    "ERROR_MESSAGE" => array(
                        "ru" => "",
                        "en" => "",
                    ),
                    "HELP_MESSAGE" => array(
                        "ru" => "",
                        "en" => "",
                    ),
                );
                $userTypeId = $userTypeEntity->Add($userTypeData);
            }
        }

        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandlerCompatible(
            "iblock",
            "OnAfterIBlockElementUpdate",
            $this->MODULE_ID,
            'iblockLoggerHandler',
            "onAfterIBlockElementUpdateHandler"
        );

    }

    public function doUninstall()
    {
        \Bitrix\Main\Loader::includeModule("highloadblock");
        $rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('=TABLE_NAME'=>'iblocklogger')));
        if ($hldata = $rsData->fetch()){
            Bitrix\Highloadblock\HighloadBlockTable::delete($hldata['ID']);
        }

        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->unregisterEventHandler(
            "iblock",
            "OnAfterIBlockElementUpdate",
            $this->MODULE_ID,
            'iblockLoggerHandler',
            "onAfterIBlockElementUpdateHandler"
        );

        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
}
