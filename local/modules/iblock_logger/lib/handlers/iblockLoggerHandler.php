<?php
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

class iblockLoggerHandler {


    public static function onAfterIBlockElementUpdateHandler(&$fields){

        \Bitrix\Main\Loader::includeModule("highloadblock");
        $rsData = HLBT::getList(array('filter'=>array('=TABLE_NAME'=>'iblocklogger')));
        if ($hldata = $rsData->fetch()){

            $hlblock = HLBT::getById($hldata['ID'])->fetch();
            $entity = HLBT::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $entity_data_class::add([
                'UF_USER_ID'    =>  $GLOBALS['USER']->GetId(),
                'UF_ELEMENT'    =>  $fields['ID'],
                'UF_TIME'       =>  \Bitrix\Main\Type\DateTime::createFromTimestamp(time())
            ]);
        }
    }
}